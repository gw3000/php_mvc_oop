<?php

// load the bootstrap file
require_once 'config/config.php';

// autoloader to load libraries in libraries folder
spl_autoload_register(function ($className) {
    require_once 'libraries/' . $className . '.php';
});
