<?php
// db params
define('DB_HOST', '_your_host');
define('DB_USER', '_your_user');
define('DB_PASS', '_your_pass');
define('DB_NAME', '_your_dbname');

// app route
define('APPROOT', dirname(dirname(__FILE__)));

// url root
define('URLROOT', '_your_url');

// define sitename
define('SITENAME', '_your_sitename');
