<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- stylesheet -->
    <link rel="stylesheet" href="<?php echo URLROOT; ?>/css/style.css">

    <!-- title from config param -->
    <title> <?php echo SITENAME; ?> </title>
</head>

<body>